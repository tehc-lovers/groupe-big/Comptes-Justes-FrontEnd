import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import {filter, Observable} from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import {User} from "../users/user";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {TokenManagerService} from "../login/token-manager.service";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit{

  connected:String = "Login";
  connectedNickName="rien";
  //selectUser:User;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  title: string = "";

  constructor(private breakpointObserver: BreakpointObserver,public tokenManagerService: TokenManagerService) {
  }

  ngOnInit(): void {
    this.title = "Application"
  }

  ngOnChanges() {
    if(this.tokenManagerService.getToken()!=null)
      this.connectedNickName=this.tokenManagerService.retrieve().nickname;
  }

  public changeTitle(event: any) {
    this.title = event.target.innerText;
  }
}

