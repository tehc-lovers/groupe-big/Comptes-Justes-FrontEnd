import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AppRoutingModule } from './app-routing.module';
import { TransactionsComponent } from './transactions/transactions.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import {MatCardModule} from "@angular/material/card";
import { LoginComponent } from './login/login.component';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { NewTransactionComponent } from './transactions/new-transaction/new-transaction.component';
import {NotFoundComponent} from "./not-found/not-found.component";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {MatMenuModule} from "@angular/material/menu";
import { UsersComponent } from './users/users.component';
import {HttpClientModule} from "@angular/common/http";
import { NewBillComponent } from './new-bill/new-bill.component';
import {MatDialogModule} from "@angular/material/dialog";
import { AccountComponent } from './account/account.component';
import { NewAccountComponent } from './account/new-account/new-account.component';
import {EventEmitterService} from "./event-emitter.service";
import { LogoutComponent } from './logout/logout.component';
import { FormComponent } from './users/form/form.component';


@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    TransactionsComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    NewTransactionComponent,
    NotFoundComponent,
    UsersComponent,
    LogoutComponent,
    FormComponent,
    NewBillComponent,
    AccountComponent,
    NewAccountComponent,
    LogoutComponent
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        LayoutModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        AppRoutingModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatCardModule,
        MatInputModule,
        MatSelectModule,
        MatRadioModule,
      MatDialogModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        MatMenuModule,
      HttpClientModule
    ],
  providers: [
    EventEmitterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
