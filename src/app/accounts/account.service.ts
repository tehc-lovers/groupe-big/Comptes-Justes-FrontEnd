import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Account} from "./account";
import {AccountComponent} from "../account/account.component";

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private static apiUrl:string=environment.apiUrl+"/accounts";
  constructor(private httpClient:HttpClient) { }

  getAll():Observable<Account[]>{
    return this.httpClient.get<Account[]>(AccountService.apiUrl);
  }

  create(account:Account):Observable<Account>{
    return this.httpClient.post<Account>(AccountService.apiUrl,account);
  }

  getAccountByName(name:string):Observable<Account[]>{
    return this.httpClient.get<Account[]>(AccountService.apiUrl+"/"+name);
  }
}
