import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {AccountUser} from "./account-user";
import {Observable} from "rxjs";
import {User} from "../users/user";
import {Account} from "./account";

@Injectable({
  providedIn: 'root'
})
export class AccountUserService {

  private static apiUrl:string=environment.apiUrl+"/accountuser"
  constructor(private httpClient:HttpClient) { }

  create(accountUser:AccountUser):Observable<AccountUser>{
    return this.httpClient.post<AccountUser>(AccountUserService.apiUrl, accountUser);
  }

  getUsersByIdAccount(idAccount:number):Observable<User[]>{
    return this.httpClient.get<User[]>(AccountUserService.apiUrl+"/IdAccount/"+idAccount);
  }

  getAccountsByIdUser(idUser:number):Observable<Account[]>{
    return this.httpClient.get<Account[]>(AccountUserService.apiUrl+"/IdUser/"+idUser);
  }
}
