import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {AccountManager} from "./account-manager";
import {Observable} from "rxjs";
import {Account} from "./account";
import {AccountUserService} from "./account-user.service";

@Injectable({
  providedIn: 'root'
})
export class AccountManagerService {

  private static apiUrl:string=environment.apiUrl+"/accountmanager";
  constructor(private htppClient:HttpClient) { }

  create(accountManager:AccountManager):Observable<AccountManager>{
    return this.htppClient.post<AccountManager>(AccountManagerService.apiUrl, accountManager);
  }

  getAccountsByIdManager(idManager:number):Observable<Account[]>{
    return this.htppClient.get<Account[]>(AccountManagerService.apiUrl+"/"+idManager);
  }
}
