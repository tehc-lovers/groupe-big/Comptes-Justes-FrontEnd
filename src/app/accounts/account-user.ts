import {Time} from "@angular/common";

export interface AccountUser {
  id?:number;
  idUser?:number;
  idAccount?:number;
  addedDate:Date;
}
