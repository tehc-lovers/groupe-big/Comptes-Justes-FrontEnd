import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {Bill} from "./bill";

@Injectable({
  providedIn: 'root'
})
export class BillService {

  private static apiUrl:string=environment.apiUrl+"/bills";

  constructor(private httpClient:HttpClient) { }

  getAll():Observable<Bill[]>{
    return this.httpClient.get<Bill[]>(BillService.apiUrl);
  }

  create(bill: Bill):Observable<Bill>{
    return this.httpClient.post<Bill>(BillService.apiUrl, bill);
  }
}
