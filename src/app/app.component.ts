import { Component } from '@angular/core';

import
{
  faAngleDoubleLeft,
  faHome,
  faUser,
  faSignInAlt,
  faCog

} from "@fortawesome/free-solid-svg-icons";

import
{
} from "@fortawesome/free-regular-svg-icons";

@Component( {
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
} )
export class AppComponent
{
  title = 'Comptes Justes';
  faAnglesLeft = faAngleDoubleLeft;
  faHome = faHome;
  faUser = faUser;
  faSignInAlt = faSignInAlt;
  faCog = faCog;
}
