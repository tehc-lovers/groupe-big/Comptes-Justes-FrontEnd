import {Component, OnInit} from '@angular/core';
import {EventBusService} from "../event-bus/event-bus.service";
import {EventType} from "../event-bus/event-type";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {BillService} from "../Bills/bill.service";
import {Bill} from "../Bills/bill";
import {Router} from "@angular/router";

@Component({
  selector: 'app-new-bill',
  templateUrl: './new-bill.component.html',
  styleUrls: ['./new-bill.component.css']
})
export class NewBillComponent implements OnInit {

  id:number=0;

  form:FormGroup=this.fb.group({
    relativePath:this.fb.control("", Validators.required)
  });

  constructor(private eventBus:EventBusService, private fb:FormBuilder,
              private billService:BillService, private router:Router) { }

  ngOnInit() {
   this.generateIdTransaction((id:number)=>{
     this.id=id;
   });
  }

  ngOnChange(){
    this.eventBus.when(EventType.MESSAGE_DISPLAYED).subscribe(idBus=>
      this.id=idBus);
    console.log(this.id);
  }

  private generateIdTransaction(callback:any){
    this.eventBus.when(EventType.MESSAGE_DISPLAYED).subscribe(id=>{
      callback(id);
    })
  }
  private createBill(bill:Bill, callback:any){
    this.billService.create(bill).subscribe(tr=>callback(tr));
  }

  sendData() {

    let bill:Bill={
      idTransaction:this.id,
      relativePath:this.form.value.relativePath
    };
    this.createBill(bill, (b:Bill)=>{

    });
    this.router.navigateByUrl("/transactions");
  }
}
