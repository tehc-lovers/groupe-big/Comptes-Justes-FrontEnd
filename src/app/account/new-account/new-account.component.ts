import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {AccountService} from "../../accounts/account.service";
import {Account} from "../../accounts/account";
import {subscribeOn} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-new-account',
  templateUrl: './new-account.component.html',
  styleUrls: ['./new-account.component.css']
})
export class NewAccountComponent implements OnInit {

  form=this.fb.group({
    name:[null, Validators.required]
  });

  constructor(private fb:FormBuilder, private accountService:AccountService,
              private router:Router) { }

  ngOnInit(): void {
  }

  createAccount(account:Account){
    this.accountService.create(account).subscribe(value => console.log(value));
  }

  onSubmit():void{
    let account:Account={
      name:this.form.value.name
    };
    this.createAccount(account);
    this.form.reset();
    this.router.navigateByUrl("/account");
  }

}
