import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AccountService} from "../accounts/account.service";
import {Account} from "../accounts/account";
import {AccountManagerService} from "../accounts/account-manager.service";
import {AccountUserService} from "../accounts/account-user.service";
import {EventBusService} from "../event-bus/event-bus.service";
import {EventType} from "../event-bus/event-type";
import {AccountTransactionService} from "../Transaction/account-transaction.service";
import {Transaction} from "../Transaction/transaction";

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  public accounts:Account[]=[];
  public accountsManaged:Account[]=[];
  public accountsUsed:Account[]=[];
  public accountsTot:Account[]=[];
  public idCompte:number=0;

  @Output() idAccountChange: EventEmitter<number>= new EventEmitter<number>();
  constructor(private accountService:AccountService,
              private accountManagerService:AccountManagerService,
              private accountUserService:AccountUserService,
              private eventBus:EventBusService,
              private accountTranactionService:AccountTransactionService) { }

  ngOnInit(): void {
    this.getAllAccount();
    this.getAccountsByIdUser(1);
    for (const account of this.accountsManaged) {
      this.accountsTot.push(account);
    }
    for(const account of this.accountsUsed){
      this.accountsTot.push(account);
    }
  }

  ngOnFocus():void{
    this.getAllAccount();
    this.getAccountsByIdUser(1);
  }
  ngOnChanges():void{
    this.getAllAccount();
    this.getAccountsByIdUser(1);
  }

  getAllAccount(){
    this.accountService.getAll().subscribe(value => this.accounts=value);
  }

  getAccountsByIdUser(idUser:number){
    this.accountManagerService.getAccountsByIdManager(idUser)
      .subscribe(value => this.accountsManaged=value);
    this.accountUserService.getAccountsByIdUser(idUser)
      .subscribe(value => this.accountsUsed=value);
  }


  modifyId(i:number) {
    this.idCompte=i;
    this.idAccountChange.next(i);
    let transac;
    this.accountTranactionService.getTransactionsByAccountId(i).subscribe(value => transac=value);
    this.eventBus.next({
      type:EventType.MESSAGE_DISPLAYED,
      data:transac
    });

    console.log(this.idCompte);
  }
}
