import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Transaction} from "./transaction";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  private static apiUrl:string=environment.apiUrl+"/transactions";

  constructor(private httpClient:HttpClient) { }

  getAll():Observable<Transaction[]>{
    return this.httpClient.get<Transaction[]>(TransactionService.apiUrl);
  }

  create(transaction:Transaction):Observable<Transaction>{
    return this.httpClient.post<Transaction>(TransactionService.apiUrl, transaction);
  }
}
