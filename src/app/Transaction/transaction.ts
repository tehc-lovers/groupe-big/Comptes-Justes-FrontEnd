export interface Transaction {
  id?:number;
  amount:number;
  date:string;
  level:number;
  communication:string;
}
