import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Account} from "../accounts/account";
import {A} from "@angular/cdk/keycodes";
import {Transaction} from "./transaction";

@Injectable({
  providedIn: 'root'
})
export class AccountTransactionService {

  private static apiUrl:string=environment.apiUrl+"/transactionAccount";
  constructor(private htppClient:HttpClient) { }

  getTransactionsByAccountId(idAccount:number):Observable<Transaction[]>{
    return this.htppClient.get<Transaction[]>(AccountTransactionService.apiUrl+"/"+idAccount);
  }
}
