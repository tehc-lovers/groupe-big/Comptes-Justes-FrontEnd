import {Component, OnInit} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {User} from "../users/user";
import {TokenManagerService} from "./token-manager.service";
import {UsersdbService} from "../users/usersdb.service";
import {UsersComponent} from "../users/users.component";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  loginForm = this.fb.group({
    username: [null, Validators.required],
    password: [null, Validators.required]
  });

  constructor(private fb: FormBuilder, private tokenManagerService:TokenManagerService, private usersdbService:UsersdbService,private route:Router) {
  }

  onSubmit(): void {
    this.usersdbService.getAll().subscribe(data => {
      for (let i = 0; i < data.length; i++) {
        if(this.loginForm.value.username == data[i].nickname && this.loginForm.value.password == data[i].password) {
          localStorage.clear();
          this.tokenManagerService.store(data[i]);
          alert("connected user : " + this.tokenManagerService.retrieve().nickname);
          this.route.navigate(['..','account']);
          return;
        }
      }
      alert("this user doesn't exist");
      this.loginForm.reset();
    });
  }

}
