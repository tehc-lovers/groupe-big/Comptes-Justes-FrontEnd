import { Injectable } from '@angular/core';
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TokenManagerService {

  public tokenKey: string = 'connected_User';

  constructor() { }

  //store the connected user
  store(content:any) {
    let contentData;

    console.log("inside localstorsge store:", content);
    contentData = content.nickname;
    console.log("contentData:", contentData)
    localStorage.setItem(this.tokenKey, JSON.stringify(content));


  }

  retrieve() : any {
    console.log("inside localstorage");
    let storedToken: any = localStorage.getItem(this.tokenKey);
    console.log("storedToken:", JSON.parse(storedToken));
    if (!storedToken) throw 'no token found';
    return JSON.parse(storedToken);
  }

  getToken(){
    return localStorage.getItem(this.tokenKey);
  }

  logout(){
    localStorage.removeItem(this.tokenKey);
    localStorage.clear();
  }
}
