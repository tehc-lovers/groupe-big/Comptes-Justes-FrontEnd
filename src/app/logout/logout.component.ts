import { Component, OnInit } from '@angular/core';
import {TokenManagerService} from "../login/token-manager.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private tokenManagerService:TokenManagerService, private route:Router) { }

  ngOnInit(): void {
  }

  logout(){
    this.tokenManagerService.logout();
    this.route.navigate(['..','login']);
  }

}
