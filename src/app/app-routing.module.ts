import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TransactionsComponent} from "./transactions/transactions.component";
import {LoginComponent} from "./login/login.component";
import {LogoutComponent} from "./logout/logout.component";
import {RegisterComponent} from "./register/register.component";
import {NewBillComponent} from "./new-bill/new-bill.component";
import {NewTransactionComponent} from "./transactions/new-transaction/new-transaction.component";
import {FormComponent} from "./users/form/form.component";
import {AccountComponent} from "./account/account.component";
import {NewAccountComponent} from "./account/new-account/new-account.component";



const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home'},
  { path: 'login', component: LoginComponent },
  {path: 'logout',component:LogoutComponent},
  { path: 'register', component: RegisterComponent },
  {path: 'userUpdate', component:FormComponent},
  { path: 'transactions', component: TransactionsComponent },
  { path: 'new-transaction', component: NewTransactionComponent },
  {path:'new-bill', component:NewBillComponent},
  {path:'account', component:AccountComponent},
  {path:'new-account', component:NewAccountComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
