import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';
import {TransactionsItem} from "./transactions-item";

const TABLE_DATA: TransactionsItem[] = [
  {billNumber: 1, billDate: '10/08/2021', billCommunication: 'Anniversary', billAmount: +250},
  {billNumber: 2, billDate: '01/09/2021', billCommunication: 'Found money on ground', billAmount: +10},
  {billNumber: 3, billDate: '15/10/2021', billCommunication: 'Gas fill up', billAmount: -65},
  {billNumber: 4, billDate: '18/12/2021', billCommunication: 'Christmas shopping', billAmount: -150},
  {billNumber: 5, billDate: '20/01/2022', billCommunication: 'Coffee', billAmount: -2.35}
];

/**
 * Data source for the Transactions view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class TransactionsDataSource extends DataSource<TransactionsItem> {
  data: TransactionsItem[] = TABLE_DATA;
  paginator: MatPaginator | undefined;
  sort: MatSort | undefined;

  constructor() {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<TransactionsItem[]> {
    if (this.paginator && this.sort) {
      // Combine everything that affects the rendered data into one update
      // stream for the data-table to consume.
      return merge(observableOf(this.data), this.paginator.page, this.sort.sortChange)
        .pipe(map(() => {
          return this.getPagedData(this.getSortedData([...this.data ]));
        }));
    } else {
      throw Error('Please set the paginator and sort on the data source before connecting.');
    }
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect(): void {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: TransactionsItem[]): TransactionsItem[] {
    if (this.paginator) {
      const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
      return data.splice(startIndex, this.paginator.pageSize);
    } else {
      return data;
    }
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: TransactionsItem[]): TransactionsItem[] {
    if (!this.sort || !this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort?.direction === 'asc';
      switch (this.sort?.active) {
        case 'billAmount': return compare(a.billAmount, b.billAmount, isAsc);
        /* case 'billDate': return compare(a.billDate, b.billDate, isAsc); -> doesn't work atm */
        case 'billCommunication': return compare(a.billCommunication, b.billCommunication, isAsc);
        case 'billNumber': return compare(a.billNumber, b.billNumber, isAsc);
        default: return 0;
      }
    });
  }

  getLastIndex(): number {
    return this.data[this.data.length - 1].billNumber;
  }
}

/** Simple sort comparator (for client-side sorting). */
function compare(a: string | number, b: string | number, isAsc: boolean): number {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
