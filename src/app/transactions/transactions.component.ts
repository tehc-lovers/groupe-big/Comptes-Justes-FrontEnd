import {AfterViewInit, Component, EventEmitter, Output, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {TransactionsDataSource} from './transactions-datasource';
import {BalanceCalculatorService} from "./balance-calculator.service";
import {Transaction} from "../Transaction/transaction";
import {TransactionService} from "../Transaction/transaction.service";
import {BillService} from "../Bills/bill.service";
import {Bill} from "../Bills/bill";
import {EventBusService} from "../event-bus/event-bus.service";
import {EventType} from "../event-bus/event-type";
import {MatDialog} from "@angular/material/dialog";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../users/user";
import {AccountTransactionService} from "../Transaction/account-transaction.service";

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})

export class TransactionsComponent implements AfterViewInit {
  private users: any;


  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  //@ViewChild(MatTable) table!: MatTable<TransactionsItem>;
  @Output() idTransaction:EventEmitter<number>=new EventEmitter<number>();
  dataSource: TransactionsDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['billNumber', 'billDate', 'billCommunication', 'billAmount'];
  amounts: number[];

  //
  // currentUser:User={
  //   nickname:"",
  //   isAdmin:false
  // };
  transactions: Transaction[] = [];
  bills:Bill[]=[];
  showing:boolean[]=[];
  idTrans:number=0;
  idAccount:number=0;
  test:Transaction[]=[];

  form:FormGroup=this.fb.group({
    relativePath:this.fb.control("", Validators.required)
  });


  constructor(private sumService: BalanceCalculatorService,
              private eventBus:EventBusService, private transactionService:TransactionService,
              private billService:BillService,
              private dialog :MatDialog, private fb:FormBuilder,
              private accountTransactionService:AccountTransactionService) {
    this.dataSource = new TransactionsDataSource(); /* Data from transactions-datasource */
    this.amounts = this.dataSource.data.map(t=>t.billAmount);

  }

  calculateSum(): number {
    let tot=0;
    for (const transaction of this.transactions) {
      tot+=transaction.amount;
    }
    return tot;
  }
  ngAfterViewInit(): void {
  }


  ngOnInit():void{
    this.getAllTransaction();
    this.getAllBill();
    this.eventBus.when(EventType.MESSAGE_DISPLAYED).subscribe(value => {
      this.miseAJourAsync(value);
    });
    for (let transaction of this.transactions){
      this.showing.push(false);
    }


  }

  ngDoCheck():void{
    this.eventBus.when(EventType.MESSAGE_DISPLAYED)
      .subscribe(value => this.idAccount=value);
  }

  ngOnFocus():void{
    this.getAllTransaction();
    this.getAllBill();

  }
  ngOnChanges():void{
    this.getAllTransaction();
    this.getAllBill();

  }
/*

  updateData(event: any) {
    let counter: number = this.dataSource.getLastIndex() + 1;
    event.billNumber = counter;
    this.dataSource.data.push(event);
    this.amounts = this.dataSource.data.map(t=>t.billAmount);
  }


  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }


  filterUsers(val:boolean):User[]{
    return this.users.filter((user:User) => user.isAdmin == val);
  }
*/
  private getAllTransaction(){

    /*this.eventBus.when(EventType.MESSAGE_DISPLAYED).subscribe(value => {
      this.miseAJourAsync(value);
    });
    setTimeout(()=>{

    }, 1000);*/

    this.transactionService.getAll().subscribe(transaction=>this.transactions=transaction);
  }

  private getAllBill(){
    this.billService.getAll().subscribe(bill=>this.bills=bill);
  }

  private createBill(bill:Bill){
    this.billService.create(bill).subscribe(res=>console.log(bill.relativePath));
  }


  miseAJourAsync(value:any){
    this.transactions=value;
    console.log(value);
  }

  showNewBill(id?:number) {
    if (id != null) {
      this.idTrans=id;
      this.eventBus.next({
        type:EventType.MESSAGE_DISPLAYED,
        data:id
      });
    }
  }

  showForm(j:number) {
    this.showing[j]=true;
  }

  hiddeForm(j:number) {
    if(this.form.value.relativePath !=""){
      let bill:Bill={
        idTransaction:j+1,
        relativePath:this.form.value.relativePath
      };
      this.form.reset();
      this.createBill(bill);
      this.ngOnChanges();
      this.ngOnInit();
    }
    this.showing[j]=false;
  }
}
