import {Component, EventEmitter, Output} from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {Router} from "@angular/router";
import {TransactionService} from "../../Transaction/transaction.service";
import {Transaction} from "../../Transaction/transaction";

@Component({
  selector: 'app-new-transaction',
  templateUrl: './new-transaction.component.html',
  styleUrls: ['./new-transaction.component.css']
})
export class NewTransactionComponent {
 // @Output() transactionChange: EventEmitter<TransactionsItem> = new EventEmitter<TransactionsItem>();

  newTransactionForm = this.fb.group({
    billAmount: [null, Validators.required],
    billDate: [null, Validators.required],
    billCommunication: [null, Validators.required]
  });

  constructor(private fb: FormBuilder, private route:Router, private transacService:TransactionService) {}

  createTransaction(transaction:Transaction, callback:any){
    this.transacService.create(transaction).subscribe(tr=>callback(tr));
  }

  onSubmit(): void {
    let transaction:Transaction={
      amount:this.newTransactionForm.value.billAmount,
      date:this.newTransactionForm.value.billDate,
      communication:this.newTransactionForm.value.billCommunication,
      level:0
    };

    this.createTransaction(transaction, (tr:Transaction)=>{
      alert(tr);
    });
    //alert(this.newTransactionForm.value.billAmount);
    this.route.navigateByUrl("/transactions");

    this.newTransactionForm.reset();
  }
}
