import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BalanceCalculatorService {

  constructor() { }


  calculate(nums: number[][]): number {
    let sum: number = 0;

    for (let i = 0; i < nums.length; i++) {
      for (let j = 0; j < nums[i].length; j++) {
        sum += nums[i][j];
      }
    }

    return sum;
  }
}
