export interface TransactionsItem {
  billAmount: number;
  billDate: string;
  billCommunication: string;
  billNumber: number;
}
