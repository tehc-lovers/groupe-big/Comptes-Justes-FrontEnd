import { Injectable } from '@angular/core';
import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "./user";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private static apiUrl:string= environment.apiUrl+"/users"
  constructor(private httpClient:HttpClient) { }

  getAll():Observable<User[]>{
    return this.httpClient.get<User[]>(UserService.apiUrl);
  }
}
