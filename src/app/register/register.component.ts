
import {Component, EventEmitter, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from "../users/user";
import {UsersComponent} from "../users/users.component";
import {EventEmitterService} from "../event-emitter.service";
import {UsersdbService} from "../users/usersdb.service";
import {Router} from "@angular/router";
import {TokenManagerService} from "../login/token-manager.service";



@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  registerForm = this.fb.group({
    firstName: [null, Validators.required],
    lastName: [null, Validators.required],
    email: [null, Validators.required],
    username: [null, Validators.required],
    password: [null, [Validators.required, Validators.minLength(8)]],
    confirmPassword: [null],
    accountNumber: [null, Validators.required]
  }, {validator: this.checkPasswords });

  constructor(private fb: FormBuilder, private eventEmitterService: EventEmitterService, private usersdbService: UsersdbService, private router:Router,private tokenManagerService : TokenManagerService) {
  }

  // Method that checks if the passwords match
  checkPasswords(group: FormGroup) {
    let pass = group.controls['password'].value;
    let confirmPass = group.controls['confirmPassword'].value;

    return pass === confirmPass ? null : { notSame: true }
  }

  onSubmit(): void {
      const localUser : User = {
        firstName : this.registerForm.getRawValue().firstName,
        lastName : this.registerForm.getRawValue().lastName,
        email : this.registerForm.getRawValue().email,
        nickname : this.registerForm.getRawValue().username,
        password : this.registerForm.getRawValue().password,
        accountNumber : this.registerForm.getRawValue().accountNumber
      };
      console.log(localUser);
      this.usersdbService.create(localUser).subscribe(user => this.eventEmitterService.onFirstComponentButtonClick(user));
      alert("addition successfully");
      if(this.tokenManagerService.getToken() == null)
        this.router.navigate(['..','login']);
      else
        this.router.navigate(['..','transactions']);
    }

}
