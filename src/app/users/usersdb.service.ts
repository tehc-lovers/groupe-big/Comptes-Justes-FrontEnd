import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "./user";

@Injectable({
  providedIn: 'root'
})
export class UsersdbService {
  private static readonly API_URL: string = environment.apiUrl+ "/users";

  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<User[]>{
    return this.httpClient.get<User[]>(UsersdbService.API_URL);
  }

  create(user:User): Observable<User>{
    return this.httpClient.post<User>(UsersdbService.API_URL, user);
  }

  update(id:number, user: User): Observable<any> {
    return this.httpClient.put(UsersdbService.API_URL+"/"+id, user);
  }
}
