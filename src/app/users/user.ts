export interface User {
  id?:number;
  firstName:String;
  lastName:String;
  email:String;
  nickname:String;
  password:String;
  accountNumber:String;
}
