
import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {User} from "./user";
import {EventEmitterService} from "../event-emitter.service";
import {TokenManagerService} from "../login/token-manager.service";
import {UsersdbService} from "./usersdb.service";
import {Observable} from "rxjs";
import {Router, RouterLink} from "@angular/router";
import {RegisterComponent} from "../register/register.component";


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
 users:User[] = [];

  currentUser:User = this.tokenManagerService.retrieve();
  selectUser:User = this.currentUser;

  constructor(private eventEmitterService: EventEmitterService , private tokenManagerService: TokenManagerService, private usersdbService: UsersdbService) {}

  ngOnInit(): void {
    if (this.eventEmitterService.subsVar==undefined) {
      this.eventEmitterService.subsVar = this.eventEmitterService.invokeFirstComponentFunction.subscribe(user => {
        this.addUser(user);
      });
    }
    this.getUsers();

  }


 /* filterUsers(val:boolean):User[]{
    return this.users.filter(user => user.isAdmin == val);
  }*/

  deleteUser(user: User):void{
    const index: number = this.users.indexOf(user);
    this.users.splice(index,1);
    alert(user.nickname +" removed");
  }

  addUser(user: User){
    this.users.push(user);
  }

  updateUser(user:User){
    this.usersdbService
      .update(user.id || -1,user)
      .subscribe();
  }

  showSelectUser(user :User):void{
    alert(user.id);
  }

  getUsers(){
    this.usersdbService.getAll().subscribe(users => this.users = users);
  }

}
