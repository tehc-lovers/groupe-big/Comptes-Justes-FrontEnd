import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {EventEmitterService} from "../../event-emitter.service";
import {UsersdbService} from "../usersdb.service";
import {Router} from "@angular/router";
import {TokenManagerService} from "../../login/token-manager.service";
import {User} from "../user";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  registerForm = this.fb.group({
    firstName: [null, Validators.required],
    lastName: [null, Validators.required],
    email: [null, Validators.required],
    username: [null, Validators.required],
    password: [null, [Validators.required, Validators.minLength(8)]],
    confirmPassword: [null],
    accountNumber: [null, Validators.required]
  }, {validator: this.checkPasswords });

  constructor(private fb: FormBuilder, private eventEmitterService: EventEmitterService, private usersdbService: UsersdbService, private router:Router,private tokenManagerService : TokenManagerService) { }

  ngOnInit(): void {
  }

  // Method that checks if the passwords match
  checkPasswords(group: FormGroup) {
    let pass = group.controls['password'].value;
    let confirmPass = group.controls['confirmPassword'].value;

    return pass === confirmPass ? null : { notSame: true }
  }

  onSubmit(): void {
    const localUser : User = {
      firstName : this.registerForm.getRawValue().firstName,
      lastName : this.registerForm.getRawValue().lastName,
      email : this.registerForm.getRawValue().email,
      nickname : this.registerForm.getRawValue().username,
      password : this.registerForm.getRawValue().password,
      accountNumber : this.registerForm.getRawValue().accountNumber
    };

    this.usersdbService.update(this.tokenManagerService.retrieve().id,localUser).subscribe();
    alert("user modified");
    this.router.navigate(['..','transactions']);
  }


}
