import {EventEmitter, Injectable} from '@angular/core';
import {Subscription} from "rxjs";
import {User} from "./users/user";

@Injectable({
  providedIn: 'root'
})
export class EventEmitterService {

  invokeFirstComponentFunction = new EventEmitter();
  // @ts-ignore
  subsVar: Subscription;

  constructor() { }

  onFirstComponentButtonClick(localUser: User) {
    this.invokeFirstComponentFunction.next(localUser);
  }
}
